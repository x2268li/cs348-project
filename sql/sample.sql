DROP TABLE IF EXISTS authors;

CREATE TABLE authors (id INT, name VARCHAR(20), email VARCHAR(20));

INSERT INTO authors (id, name) VALUES (1, "bob"), (2, "steve"), (3, "phil");

SELECT * FROM authors;