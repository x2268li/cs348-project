## CS348 Course Project

### Before Beginning
Download MySQL & Node
Fill out the credentials in `test.js` -
```
  password: "YOUR_PASSWORD_HERE",
  database: "YOUR_DATABASE_HERE",
```
You will have to set these up locally.

### To Create & Load

We will be setting up a React <> Node/Express <> MySQL application for this project. For now, we set up a simple connection between Node and MySQL. 

The table is created and data is loaded in through the queries in `sql/sample.sql`. As we progress, we will investigate saving the data in another format (eg. CSV) and loading it in via command-line when the application starts. 

Currently, to run, just go to the folder and type 
```
node test.js
```

to run (assuming Node.js is installed) 

Right now, you can see this in the console output: 

![log](sample.png)