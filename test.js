var mysql = require('mysql');
const fs = require("fs");

var con = mysql.createConnection({
  host: "localhost",
  user: "root",
  password: "YOUR_PASSWORD_HERE",
  database: "YOUR_DATABASE_HERE",
  multipleStatements: true
});

const dataSql = fs.readFileSync("./sql/sample.sql").toString();

con.connect(function(err) {
  if (err) throw err;
  con.query(dataSql, function (err, result, fields) {
    if (err) throw err;
    console.log(result);
  });
});